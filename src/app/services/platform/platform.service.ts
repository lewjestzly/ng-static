import {Injectable, Optional} from '@angular/core';
import {PLATFORM_ID, APP_ID, Inject} from '@angular/core';
import {APP_BASE_HREF, isPlatformBrowser, isPlatformServer} from '@angular/common';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PlatformService {

    protected apiOrigin;

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        @Inject(APP_ID) private appId: string) {
        this.apiOrigin = this.isServer() ? environment.apiServerOrigin : environment.apiBrowserOrigin;
    }

    isBrowser() {
        return isPlatformBrowser(this.platformId);
    }

    isServer() {
        return isPlatformServer(this.platformId);
    }

    getApiOrigin() {
        return this.apiOrigin;
    }
}
