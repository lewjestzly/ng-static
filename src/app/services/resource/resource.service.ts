import {Injectable} from '@angular/core';
import {PlatformService} from '../platform/platform.service';
import {HttpClient} from '@angular/common/http';
import {makeStateKey, StateKey, TransferState} from '@angular/platform-browser';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';

const NUMBERS_KEY = makeStateKey<number[]>('numbers');

@Injectable({
    providedIn: 'root'
})
export class ResourceService {

    constructor(private platformService: PlatformService, private http: HttpClient, private transferState: TransferState) {
    }

    private getOrTransfer<T>(key: StateKey<T>, url: string): Observable<T> {
        const exists = this.transferState.hasKey(key);
        if (exists) {
            const res = of(this.transferState.get<T>(key, null));
            this.transferState.remove(key);
            return res;
        } else {
            return this.http.get<T>(url).pipe(tap((success) => {
                if (this.platformService.isServer()) {
                    this.transferState.set(key, success);
                }
            }));
        }
    }

    getNumbers() {
        return this.getOrTransfer<number[]>(NUMBERS_KEY, `${this.platformService.getApiOrigin()}/api/numbers`);
    }
}
