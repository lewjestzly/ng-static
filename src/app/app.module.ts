import {BrowserModule, BrowserTransferStateModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ContactComponent} from './components/contact/contact.component';
import {HomeComponent} from './components/home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {TransferHttpCacheModule} from '@nguniversal/common';

@NgModule({
    declarations: [
        AppComponent,
        ContactComponent,
        HomeComponent
    ],
    imports: [
        BrowserModule.withServerTransition({appId: 'ng-static'}),
        AppRoutingModule,
        TransferHttpCacheModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
