export const environment = {
    production: true,
    apiBrowserOrigin: 'http://localhost:4000',
    apiServerOrigin: 'http://localhost:4000'
};
