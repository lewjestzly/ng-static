import {server} from './server-instance/web-server';

server.run().then((PORT) => {
    console.log(`Node server listening on http://localhost:${PORT}`); //
});
